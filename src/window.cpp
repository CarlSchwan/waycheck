#include "window.hpp"

#include "protocols.hpp"
#include "qstandarditemmodel.h"
#include "ui_window.h"

#include <QAbstractItemView>
#include <QCheckBox>
#include <QGuiApplication>
#include <QStandardItemModel>
#include <fstream>
#include <sys/socket.h>
#include <wayland-client-protocol.h>
#include <wayland-client.h>

static pid_t pid_from_fd(int fd) {
    struct ucred ucred;
    socklen_t len = sizeof(struct ucred);
    if (getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &ucred, &len) == -1) {
        perror("getsockopt failed");
        exit(1);
    }
    return ucred.pid;
}

static std::string process_name_from_pid(const pid_t pid) {
    std::string procpath = QString::asprintf("/proc/%d/comm", pid).toStdString();

    std::ifstream infile(procpath);
    if (infile.is_open()) {
        std::string out;
        std::getline(infile, out);
        infile.close();
        return out;
    } else {
        return "Unknown";
    }
}

static void registry_global(void* data, wl_registry* registry, uint32_t name, const char* interface, uint32_t version) {
    (void) registry;
    (void) name;

    if (std::string(interface).starts_with("wl_")) {
        return;
    }

    auto* window = static_cast<Window*>(data);
    window->newGlobal(interface, version);
}

static void registry_global_remove(void* data, struct wl_registry* registry, uint32_t name) {
    (void) data;
    (void) registry;
    (void) name;
}

Window::Window(QNativeInterface::QWaylandApplication* waylandApp, QWidget* parent)
    : QMainWindow(parent), upstreamModel(0, 4), wlrootsModel(0, 3), kdeModel(0, 3), westonModel(0, 3), ui(new Ui::Window) {
    ui->setupUi(this);

    initTable(upstreamModel, *ui->upstreamTable);
    initTable(wlrootsModel, *ui->wlrootsTable, false);
    initTable(kdeModel, *ui->kdeTable, false);
    initTable(westonModel, *ui->westonTable, false);

    for (auto protocol : known_protocols) {
        addProtocol(protocol.second);
    }

    ui->upstreamTable->sortByColumn(0, Qt::AscendingOrder);
    ui->wlrootsTable->sortByColumn(0, Qt::AscendingOrder);
    ui->kdeTable->sortByColumn(0, Qt::AscendingOrder);
    ui->westonTable->sortByColumn(0, Qt::AscendingOrder);

    auto* display = waylandApp->display();
    auto fd = wl_display_get_fd(display);
    auto pid = pid_from_fd(fd);
    auto pname = process_name_from_pid(pid);
    ui->compositor->setText(QString::asprintf("Compositor: %s", pname.c_str()));

    auto listener = wl_registry_listener{.global = registry_global, .global_remove = registry_global_remove};

    auto* registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &listener, (void*) this);
    wl_display_roundtrip(display);
}

Window::~Window() {
    delete ui;
}

void Window::newGlobal(const std::string interface, const uint32_t version) {
    const Protocol& protocol = known_protocols[interface];
    if (protocol.status == UNKNOWN) {
        return;
    }

    auto* model = modelForStatus(protocol.status);
    if (model == nullptr) {
        return;
    }

    auto column = (model == &upstreamModel) ? 2 : 1;
    auto matches = model->findItems(QString::fromStdString(protocol.name), Qt::MatchExactly, column);

    for (auto match : matches) {
        auto versionItem = model->item(match->row(), column + 1);
        versionItem->setCheckState(Qt::Checked);
        versionItem->setText(QString::asprintf("%d", version));

        for (auto i = 0; i < model->columnCount(); i++) {
            auto item = model->item(match->row(), i);
            auto bg = item->background();
            bg.setStyle(Qt::NoBrush);
            item->setBackground(bg);
        }
    }
}

void Window::addProtocol(const Protocol& protocol) {
    auto* model = modelForStatus(protocol.status);
    if (model == nullptr) {
        return;
    }

    auto versionItem = new QStandardItem(false);
    versionItem->setCheckState(Qt::Unchecked);
    versionItem->setText("N/A");

    auto items = QList<QStandardItem*>({
        new QStandardItem(QString::fromStdString(protocol.pretty)),
        new QStandardItem(QString::fromStdString(protocol.name)),
        versionItem,
    });
    if (protocol.status <= UNSTABLE) {
        auto* statusItem = new QStandardItem(QString::fromStdString(status_to_string(protocol.status)));
        statusItem->setTextAlignment(Qt::AlignCenter);
        items.prepend(statusItem);
    }

    for (auto item : items) {
        auto bg = item->background();
        bg.setStyle(Qt::Dense4Pattern);
        item->setBackground(bg);
    }

    model->appendRow(items);
}

void Window::initTable(QStandardItemModel& model, QTableView& table, bool includeStatus) {
    table.setModel(&model);
    table.setSortingEnabled(true);

    if (includeStatus) {
        model.setHorizontalHeaderLabels({"Status", "Name", "Identifier", "Implementation"});
        table.horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        table.horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
        table.horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
        table.horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    } else {
        model.setHorizontalHeaderLabels({"Name", "Identifier", "Implementation"});
        table.horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
        table.horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
        table.horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    }
    table.sortByColumn(0, Qt::AscendingOrder);
}

QStandardItemModel* Window::modelForStatus(const ProtocolStatus status) {
    switch (status) {
        case STABLE:
        case STAGING:
        case UNSTABLE:
            return &upstreamModel;
        case WLROOTS:
            return &wlrootsModel;
        case KDE:
            return &kdeModel;
        case WESTON:
            return &westonModel;
        default:
            return nullptr;
    }
}
