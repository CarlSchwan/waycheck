#ifndef WAYCHECK_PROTOCOLS_HPP
#define WAYCHECK_PROTOCOLS_HPP

#include <string>
#include <unordered_map>

enum ProtocolStatus {
    UNKNOWN,
    STABLE,
    STAGING,
    UNSTABLE,
    WLROOTS,
    KDE,
    WESTON,
};

struct Protocol {
    ProtocolStatus status;
    std::string pretty;
    std::string name;
};

// clang-format off
static std::unordered_map<std::string, const Protocol> known_protocols = {
    {"wp_presentation", {STABLE, "Presentation time", "presentation-time"}},
    {"wp_viewporter", {STABLE, "Viewporter", "viewporter"}},
    {"xdg_wm_base", {STABLE, "XDG shell", "xdg-shell"}},

    {"xdg_activation_v1", {STAGING, "XDG activation", "xdg-activation-v1"}},
    {"wp_drm_lease_v1", {STAGING, "DRM lease", "drm-lease-v1"}},
    {"ext_session_lock_manager_v1", {STAGING, "Session lock", "ext-session-lock-v1"}},
    {"wp_single_pixel_buffer_manager_v1", {STAGING, "Single-pixel buffer", "single-pixel-buffer-v1"}},
    {"wp_content_type_manager_v1", {STAGING, "Content type hint", "content-type-v1"}},
    {"ext_idle_notifier_v1", {STAGING, "Idle notify", "ext-idle-notify-v1"}},
    {"wp_tearing_control_manager_v1", {STAGING, "Tearing control", "tearing-control-v1"}},
    {"xwayland_shell_v1", {STAGING, "Xwayland shell", "xwayland-shell-v1"}},
    {"wp_fractional_scale_manager_v1", {STAGING, "Fractional scale", "fractional-scale-v1"}},
    {"wp_cursor_shape_manager_v1", {STAGING, "Cursor shape", "cursor-shape-v1"}},
    {"ext_foreign_toplevel_list_v1", {STAGING, "Foreign toplevel list", "ext-foreign-toplevel-list-v1"}},
    {"wp_security_context_manager_v1", {STAGING, "Security context", "security-context-v1"}},

    {"zwp_fullscreen_shell_v1", {UNSTABLE, "Fullscreen shell", "fullscreen-shell-unstable-v1"}},
    {"zwp_idle_inhibit_manager_v1", {UNSTABLE, "Idle inhibit", "idle-inhibit-unstable-v1"}},
    {"zwp_input_method_context_v1", {UNSTABLE, "Input method", "input-method-unstable-v1"}},
    {"zwp_input_timestamps_manager_v1", {UNSTABLE, "Input timestamps", "input-timestamps-unstable-v1"}},
    {"zwp_keyboard_shortcuts_inhibit_manager_v1", {UNSTABLE, "Keyboard shortcuts inhibit", "keyboard-shortcuts-inhibit-unstable-v1"}},
    {"zwp_linux_dmabuf_v1", {UNSTABLE, "Linux DMA-BUF", "linux-dmabuf-unstable-v1"}},
    {"zwp_linux_explicit_synchronization_v1", {UNSTABLE, "Linux explicit synchronization", "linux-explicit-synchronization-unstable-v1"}},
    {"zwp_pointer_constraints_v1", {UNSTABLE, "Pointer constraints", "pointer-constraints-unstable-v1"}},
    {"zwp_pointer_gestures_v1", {UNSTABLE, "Pointer gestures", "pointer-gestures-unstable-v1"}},
    {"zwp_primary_selection_device_manager_v1", {UNSTABLE, "Primary selection", "primary-selection-unstable-v1"}},
    {"zwp_relative_pointer_manager_v1", {UNSTABLE, "Relative pointer", "relative-pointer-unstable-v1"}},
    {"zwp_tablet_manager_v2", {UNSTABLE, "Tablet", "tablet-unstable-v2"}},
    {"zwp_text_input_v3", {UNSTABLE, "Text input", "text-input-unstable-v3"}},
    {"zxdg_decoration_manager_v1", {UNSTABLE, "XDG decoration", "xdg-decoration-unstable-v1"}},
    {"zxdg_exporter_v2", {UNSTABLE, "XDG foreign", "xdg-foreign-unstable-v2"}},
    {"zxdg_output_manager_v1", {UNSTABLE, "XDG output", "xdg-output-unstable-v1"}},
    {"zwp_xwayland_keyboard_grab_manager_v1", {UNSTABLE, "Xwayland keyboard grabbing", "xwayland-keyboard-grab-unstable-v1"}},

    {"zwlr_data_control_manager_v1", {WLROOTS, "Data control", "wlr-data-control-unstable-v1"}},
    {"zwlr_export_dmabuf_manager_v1", {WLROOTS, "Export DMA-BUF", "wlr-export-dmabuf-unstable-v1"}},
    {"zwlr_foreign_toplevel_manager_v1", {WLROOTS, "Foreign toplevel management", "wlr-foreign-toplevel-management-unstable-v1"}},
    {"zwlr_gamma_control_manager_v1", {WLROOTS, "Gamma control", "wlr-gamma-control-unstable-v1"}},
    {"zwlr_input_inhibit_manager_v1", {WLROOTS, "Input inhibitor", "wlr-input-inhibitor-unstable-v1"}},
    {"zwlr_layer_shell_v1", {WLROOTS, "Layer shell", "wlr-layer-shell-unstable-v1"}},
    {"zwlr_output_manager_v1", {WLROOTS, "Output management", "wlr-output-management-unstable-v1"}},
    {"zwlr_output_power_manager_v1", {WLROOTS, "Output power management", "wlr-output-power-management-unstable-v1"}},
    {"zwlr_screencopy_manager_v1", {WLROOTS, "Screencopy", "wlr-screencopy-unstable-v1"}},
    {"zwlr_virtual_pointer_v1", {WLROOTS, "Virtual pointer", "wlr-virtual-pointer-unstable-v1"}},

    {"org_kde_kwin_appmenu_manager", {KDE, "AppMenu", "appmenu"}},
    {"org_kde_kwin_blur_manager", {KDE, "Blur", "blur"}},
    {"org_kde_kwin_contrast_manager", {KDE, "Contrast", "contrast"}},
    {"org_kde_kwin_dpms_manager", {KDE, "DPMS", "dpms"}},
    {"org_kde_kwin_fake_input", {KDE, "Fake input", "fake-input"}},
    {"org_kde_kwin_idle", {KDE, "Idle", "idle"}},
    {"org_kde_kwin_keystate", {KDE, "Key state", "keystate"}},
    {"kde_lockscreen_overlay_v1", {KDE, "Lockscreen overlay", "kde-lockscreen-overlay-v1"}},
    {"org_kde_kwin_outputmanagement", {KDE, "Output management", "output-management"}},
    {"kde_output_management_v2", {KDE, "Output management v2", "kde-output-management-v2"}},
    {"org_kde_kwin_outputdevice", {KDE, "Output device", "outputdevice"}},
    {"kde_output_device_v2", {KDE, "Output device v2", "kde-output-device-v2"}},
    {"kde_output_order_v1", {KDE, "Output order", "kde-output-order-v1"}},
    {"org_kde_plasma_shell", {KDE, "Plasma shell", "plasma-shell"}},
    {"org_kde_plasma_virtual_desktop_management", {KDE, "Plasma virtual desktop", "plasma-virtual-desktop"}},
    {"org_kde_plasma_window_management", {KDE, "Plasma window management", "plasma-window-management"}},
    {"kde_primary_output_v1", {KDE, "Primary output", "kde-primary-output-v1"}},
    {"zkde_screencast_unstable_v1", {KDE, "Screencast", "zkde-screencast-unstable-v1"}},
    {"org_kde_kwin_server_decoration_manager", {KDE, "Server decoration", "server-decoration"}},
    {"org_kde_kwin_server_decoration_palette_manager", {KDE, "Server decoration palette", "server-decoration-palette"}},
    {"org_kde_kwin_shadow_manager", {KDE, "Shadow", "shadow"}},
    {"org_kde_kwin_slide_manager", {KDE, "Slide", "slide"}},

    {"ivi_application", {WESTON, "In-vehicle infotainment application", "ivi-application"}},
    {"ivi_hmi_controller", {WESTON, "In-vehicle infotainment HMI controller", "ivi-hmi-controller"}},
    {"text_cursor_position", {WESTON, "Text cursor position", "text-cursor-position"}},
    {"weston_content_protection", {WESTON, "Content protection", "weston-content-protection"}},
    {"weston_desktop_shell", {WESTON, "Desktop shell", "weston-desktop-shell"}},
    {"weston_direct_display_v1", {WESTON, "Direct display", "weston-direct-display"}},
    {"weston_capture_v1", {WESTON, "Output capture", "weston-output-capture"}},
    {"weston_screenshooter", {WESTON, "Screenshooter", "weston-screenshooter"}},
    {"weston_touch_calibration", {WESTON, "Touch calibration", "weston-touch-calibration"}},
};
// clang-format on

std::string status_to_string(ProtocolStatus status);

#endif
