#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "protocols.hpp"

#include <QGuiApplication>
#include <QMainWindow>
#include <QStandardItemModel>
#include <QTableView>

QT_BEGIN_NAMESPACE
namespace Ui {
class Window;
}
QT_END_NAMESPACE

class Window : public QMainWindow {
    Q_OBJECT
    QStandardItemModel upstreamModel;
    QStandardItemModel wlrootsModel;
    QStandardItemModel kdeModel;
    QStandardItemModel westonModel;

  public:
    Window(QNativeInterface::QWaylandApplication* waylandApp, QWidget* parent = nullptr);
    ~Window();

    void newGlobal(const std::string interface, const uint32_t version);

    void addProtocol(const Protocol& protocol);

  private:
    void initTable(QStandardItemModel& model, QTableView& table, bool includeStatus = true);

    QStandardItemModel* modelForStatus(const ProtocolStatus status);

  private:
    Ui::Window* ui;
};
#endif
