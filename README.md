# Waycheck

Waycheck is a simple Qt6 application that displays all protocols implemented by the compositor that it's running in.

## License

Waycheck is licensed under the `Apache-2.0` open-source license.
